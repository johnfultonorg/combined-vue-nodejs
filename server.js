const express = require("express");
const cors = require("cors");
const path = __dirname + '/app/views/';


const app = express();

var corsOptions = {
  //origin: "http://localhost:8081"
  origin: "*"
};

app.use(cors(corsOptions));
app.use(express.static(path));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.get('/', function (req,res) {
  res.sendFile(path + "index.html");
});

require("./app/routes/tutorial.routes.js")(app);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});