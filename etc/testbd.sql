
DROP TABLE IF EXISTS tutorials;
DROP TABLE IF EXISTS users;


CREATE TABLE IF NOT EXISTS tutorials (
  id int PRIMARY KEY AUTO_INCREMENT,
  title varchar(255) NOT NULL,
  description varchar(255) default '',
  published BOOLEAN DEFAULT false
);

CREATE TABLE IF NOT EXISTS users (
	id int  PRIMARY KEY AUTO_INCREMENT,
	username varchar(50) NOT NULL,
	password_hash varchar(200) NOT NULL,
    salt varchar(200) NOT NULL,
	role varchar(50) NOT NULL
);

DESCRIBE tutorials;
DESCRIBE users;

INSERT INTO users (username, password_hash, salt, role) VALUES ('johnfultonorg', '', '', 'user');
INSERT INTO users SET (username, password_hash, salt, role) VALUES  ('johnfultonorg1', 'd1917139d699b7bfb9222708a039f3c3e6bac39f70c977a0ceef0e1c760aac6133a650ab9126462fe3e7eec3762ac16fd8c1ecaec8691ae38f4c11c9711c824a', 'fb700660ad744a79', 'user');

SELECT * FROM users;
