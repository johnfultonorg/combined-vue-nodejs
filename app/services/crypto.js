const crypto = require('crypto');

exports.generateSaltAndHash = (password, user) => {

    //console.log("in crypto.js at generateSaltAndHash")
    // console.log(password)
    // console.log(user)
    let salt = genRandomString(16);
    let hash = sha512(password, salt);

    user.salt = salt;
    user.passwordHash = hash.passwordHash;

    //console.log(user);
    return user;
}

exports.checkPassword = (password, user) => {
    let testHash = sha512(password, user.salt);
    //console.log(user)
    //console.log(user.passwordHash)
    //console.log(testHash)
    if (testHash.passwordHash === user.passwordHash) {
        return true;
    } else {
        return false;
    }
}

/**
 * generates random string of characters i.e salt
 * https://ciphertrick.com/salt-hash-passwords-using-nodejs-crypto/
 * @function
 * @param {number} length - Length of the random string.
 */
function genRandomString(length) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length);   /** return required number of characters */
}

/**
 * hash password with sha512.
 * https://ciphertrick.com/salt-hash-passwords-using-nodejs-crypto/
 * @function
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
function sha512(password, salt) {
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
}
