const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');

// get config vars
dotenv.config();

// access config var
process.env.TOKEN_SECRET;

exports.generateAccessToken = (user) => {
    const token = jwt.sign({sub:user.userId, role: user.role, name: user.username}, 
        process.env.TOKEN_SECRET, { expiresIn: '1800s' });
        //console.log(token);
    return token;
}
