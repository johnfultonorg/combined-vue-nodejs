const Auth = require("../models/auth.model.js");
const User = require("../models/user.model.js");
const Crypto = require("../services/crypto.js")
const JWT = require("../services/jwt.js")

// Register a new user
exports.register = (req, res) => {
    //console.log("Reached register in auth.controller.js")
    //console.log(req.body);

    // Create an Auth
    const auth = new Auth({
        username: req.body.username,
        password: req.body.password,
        confirmPassword: req.body.confirmPassword
    });

    // Validate request
    if (!req.body || !req.body.username) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    //Confirm that passwords match
    else if (auth.password != auth.confirmPassword) {
        res.status(400).send({
            message: "Passwords did not match."
        });
        return;
    }

    // Confirm that this user doesn't already exist
    // we continue only after that response
    else {
        User.findByUsername(auth.username, (err, data) => {
            if (err) {
                res.status(500).send({
                    message: "Error retrieving User with username " + auth.username
                })
            } else {
                // if a user was found, this is a client error

                //console.log("in auth.controller.js at findByUsername data is ");
                //console.log(data);
                if (data) {
                    res.status(400).send({
                        message: "There is already a user with username  " + auth.username
                    })
                } else {  // continue

                    // create a user object
                    let user = {
                        username: auth.username,
                        passwordHash: "",
                        salt: "",
                        role: "user"
                    }

                    //create a hash and salt
                    user = Crypto.generateSaltAndHash(auth.password, user);
                    //console.log(user);

                    //store the user
                    Auth.register(user, (err, data) => {
                        if (err) {
                            res.status(500).send({
                                message: "Error registering User with username " + user.username
                            })
                        } else {
                            //console.log("added user " + user.username)
                            //console.log(data);



                            res.send({});

                        }
                    })


                }
            }
        })
    }
}


// login user and return jwt
exports.login = (req, res) => {

    //console.log("Reached login in auth.controller.js")
    //console.log(req.body);

    // Create a User
    const user = new User({
        username: req.body.username,
    });

    // Validate request
    if (!req.body || !req.body.username || !req.body.password) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    //get user
    else {
        User.findByUsername(user.username, (err, data) => {
            if (err) {
                res.status(500).send({
                    message: "Error retrieving User with username " + user.username
                })
            } else if (!data) {
                res.status(400).send({
                    message: "Invalid ID or password (but we really know it's the ID " + user.username + ")"
                })
            } else {
                //console.log(data);

                user.userId = data.id
                user.passwordHash = data.password_hash
                user.salt = data.salt
                user.role = data.role

                //console.log(user);

                //check the passwoprd
                if (!Crypto.checkPassword(req.body.password, user)) {
                    res.status(400).send({
                        message: "Invalid ID or password (but we really know it's the password)"
                    })
                } else {

                    //create a token
                    const token = JWT.generateAccessToken(user);
                    //console.log(token)

                    //don't sent these back to client
                    user.passwordHash = null;
                    user.salt = null;
                    //console.log(user)
                    //console.log(token)
                    res.send({ user, token });
                }
            }
        })
    }
}
