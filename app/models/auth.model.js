const sql = require("./db.js");

// constructor
const Auth = function (auth) {
    this.username = auth.username;
    this.password = auth.password;
    this.confirmPassword = auth.confirmPassword;
};

Auth.register = (newUser, result) => {
    //console.log("reached Auth.register in auth.model.js")
    //console.log(newUser)
    sql.query(`INSERT INTO users (username, password_hash, salt, role) VALUES ` +
        ` ('${newUser.username}', '${newUser.passwordHash}', '${newUser.salt}', '${newUser.role}');`, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            //console.log("created user: ", { id: res.insertId, ...newUser });
            result(null, { id: res.insertId, ...newUser });
        });
};

Auth.login = (id, result) => {
    console.log("reached Auth.register in auth.model.js")
    //   sql.query(`SELECT * FROM tutorials WHERE id = ${id}`, (err, res) => {
    //     if (err) {
    //       console.log("error: ", err);
    //       result(err, null);
    //       return;
    //     }

    //     if (res.length) {
    //       console.log("found tutorial: ", res[0]);
    //       result(null, res[0]);
    //       return;
    //     }

    //     // not found Tutorial with the id
    //     result({ kind: "not_found" }, null);
    //   });
};



module.exports = Auth;