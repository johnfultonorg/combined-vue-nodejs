const sql = require("./db.js");

const empty = {};

// constructor
const User = function (user) {
    this.userId = user.userId;
    this.username = user.username;
    this.passwordHash = user.passwordHash;
    this.salt = user.salt;
    this.role = user.role;
};


User.findByUsername = (username, result) => {

    //console.log("Reached findByUsername in user.model.js with user " + username)
    // todo is this allowing sql injection??
    sql.query(`SELECT * FROM users WHERE username = '${username}'`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        //console.log("In user.model.js at User.finduserByName found " + res.length)
        //console.log(res)

        if (res.length) {
            //console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        } else {
            result(null, null);
            return;
        }
    });
};

module.exports = User;