import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  //mode: "history",
  routes: [
    {
      path: "/",
      alias: "/tutorials",
      name: "tutorials",
      component: () => import("../components/TutorialsList.vue")
    },
    {
      path: "/add",
      name: "add",
      component: () => import("../components/AddTutorial")
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../components/Login.vue")
    },

    {
      path: "/logout",
      name: "logout",
      component: () => import("../components/Logout.vue")
    },

    {
      path: "/register",
      name: "register",
      component: () => import("../components/Register.vue")
    },

    {
      path: "/tutorials/:id",
      name: "tutorial-details",
      component: () => import("../components/Tutorial")
    },

  ]
});