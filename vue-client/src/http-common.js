import axios from "axios";

export default axios.create({
  baseURL: " https://combined-vue-nodejs.herokuapp.com/api",
  headers: {
    "Content-type": "application/json"
  }
});